<?php

  namespace App\DataTables;

  use App\Models\Category;
  use Hexters\Ladmin\Datatables\Datatables;
  use Hexters\Ladmin\Contracts\DataTablesInterface;

  class CategoryDataTables extends Datatables implements DataTablesInterface {

    /**
     * Datatables function
     */
    public function render() {

      /**
       * Data from controller
       */
      $data = self::$data;

      return $this->eloquent(Category::query())
          ->addColumn('action', function($item) {
              return view('category::livewire.list_category', [
                  'show' => null,
                  'edit' => [
                      'gate' => 'administrator.category',
                      'url' => route('administrator.category', [$item->id, 'back' => request()->fullUrl()])
                  ]
              ]);
          })
        ->escapeColumns([])
        ->make(true);
    }

    /**
     * Datatables Option
     */
    public function options() {

      /**
       * Data from controller
       */
      $data = self::$data;

      return [
        'title' => 'List Of Category',
        'buttons' => null, // e.g : view('user.actions.create')
        'fields' => [ __('ID'), ], // Table header
        'foos' => [ // Custom data array. You can call in your blade with variable $foos
          'bar' => 'baz',
          'baz' => 'bar',
        ],
        /**
         * DataTables Options
         */
        'options' => [
          'processing' => true,
          'serverSide' => true,
          'ajax' => request()->fullurl(),
          'columns' => [
              ['data' => 'id', 'class' => 'text-center'],
          ]
        ]
      ];

    }

  }
