<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'product';
    protected $fillable = ['name', 'sale_price', 'category_id', 'market_price', 'summary','description', 'manufacturer','image'];
    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }
}
