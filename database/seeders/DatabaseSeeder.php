<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Hexters\Ladmin\Models\Role;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Models\Role::first();
        \App\Models\User::factory(10)->create()
            ->each(function($user) use ($role) {
                $user->roles()->sync([$role->id]);
            });

        // \App\Models\User::factory(10)->create();
    }
}
