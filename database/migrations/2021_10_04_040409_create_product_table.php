<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name',128)->default(null);
            $table->integer('category_id')->default(0);
            $table->text('image')->default(null)->nullable();
            $table->double('sale_price')->default(0)->nullable();
            $table->double('market_price')->default(0)->nullable();
            $table->text('summary')->default(null)->nullable();
            $table->text('description')->default(null)->nullable();
            $table->text('manufacturer')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
