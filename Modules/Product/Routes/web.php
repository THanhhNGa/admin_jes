<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('administrator/product')->name('product.')->group(function(){
    Route::get('/', [\Modules\Product\Http\Livewire\ListProduct::class,'__invoke'])->name('list_product');
    Route::get('/create', [\Modules\Product\Http\Livewire\CreateProduct::class,'__invoke'])->name('create_product');
});
