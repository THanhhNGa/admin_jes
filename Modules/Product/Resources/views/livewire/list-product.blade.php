<div>
    <x-slot name="buttons">List Product</x-slot>
    <div class="list-product">
        <div class="wrap-product">
            <div class="option-product ">
                <div class="title"><label>Danh sách sản phẩm</label></div>
                <div class="option-1 ">
                    <a href="{{route('product.create_product')}}" class="create btn btn-primary" title="create">Create</a>
                </div>
            </div>
        </div>
        @if(session('alert-success'))
            <div class="alert-success bg-blue-300 border-t-4 border-blue-500 rounded-b text-white px-4 py-3 shadow-md mb-2"
                 role="alert">
                <div class="flex">
                    {{--                    <div class="icon-done">{!! showIcon('done') !!}</div>--}}
                    <div>
                        <p class="font-bold pl-1">{{session('alert-success')}}</p>
                    </div>
                </div>
            </div>
        @endif
        <div class="main-product ">
            <div class="main-content">
                <div class="item-product">
                    <div class="search">
                        <p>Show 10 entries</p>
                    </div>
                    <table class=" table-auto">
                        <thead>
                        <tr>
                            <th class="">STT</th>
                            <th class="col-name ">Tên sản phẩm</th>
                            <th class="col-name ">Tên danh mục</th>
                            <th class="col-name ">Giá sản phẩm</th>
                            <th class="col-name ">Giá thị trường</th>
                            <th class="col-name ">Tóm tắt</th>
                            <th class="col-option ">Tùy chọn</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dataProduct as $key=>$value)
                            <tr>
                                <td>{{$loop->index +1}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->category->name}}</td>
                                <td>{{$value->sale_price}}</td>
                                <td>{{$value->market_price}}</td>
                                <td>{{$value->summary}}</td>
                                <td >
                                    <a href="{{route('product.create_product',['record_id'=>$value->id])}}" class="btn btn-info">Edit</a>
                                    <button type="button" wire:click.prevent="turnModal({{$value->id}})" class="btn btn-primary"
                                    >Delete</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @if($turnModal)
            <x-product::product.Form.modal wire:model="turnModal"/>
        @endif
    </div>
</div>
