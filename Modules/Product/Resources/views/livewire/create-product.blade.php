<div>
    <x-slot name="title">Create Category</x-slot>
    <div class="create-product">
        <div class="wrap-product">
            <div class="option-product ">
                <div class="option-1 ">
                </div>
            </div>
        </div>
        <div class="main-product ">
            @if(session('alert-error'))
                <div class="bg-red-300 border-t-4 border-red-500 rounded-b text-white px-4 py-3 shadow-md mb-2"
                     role="alert">
                    <div class="flex">
                        <div>
                            <p class="font-bold pl-1">{{session('alert-error')}}</p>
                        </div>
                    </div>
                </div>
            @endif
            <div class="main-content">
                <div class="item-product">
                    <div class="row-attribute row-1">
                        <div class="title">Tên sản phẩm</div>
                        <div class="input-product ">
                            <input wire:model="name" type="text" id="name" name="name">
                        </div>
                    </div>
                    <div class="row-attribute">
                        <div class="title">Giá sản phẩm</div>
                        <div class="input-product ">
                            <input wire:model="sale_price" type="text" id="sale_price" name="sale_price">
                        </div>
                    </div>
                    <div class="row-attribute">
                        <div class="title">Giá trị trường</div>
                        <div class="input-product ">
                            <input wire:model="market_price" type="text" id="market_price" name="market_price">
                        </div>
                    </div>
                    <div class="row-attribute">
                        <div class="title">Tóm tắt</div>
                        <div class="input-product ">
                            <input wire:model="summary" type="text" id="summary" name="summary">
                        </div>
                    </div>
                    <div class="row-attribute">
                        <div class="title">Nhà sản xuất</div>
                        <div class="input-product ">
                            <input wire:model="manufacturer" type="text" id="manufacturer" name="manufacturer">
                        </div>
                    </div>
                    <div class="row-attribute">
                        <div class="title">Mô tả</div>
                        <div class="input-product">
                            <textarea wire:model="description" type="text" id="description" name="description"></textarea>
                        </div>
                    </div>
                    <div class="row-attribute">
                        <div class="title">Danh mục</div>
                        <div class="input-product">
                            <select wire:model="category_id">
                                <option>---Chọn danh mục</option>
                                @foreach($dataCate as $key=>$value)
                                <option value="{{$value->id}}" id="{{$key}}">{{$value->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row-attribute">
                        <button type="submit"  wire:click.prevent="formSubmit" class="btn btn-info">Create</button>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
