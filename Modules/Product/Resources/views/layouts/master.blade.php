<x-ladmin-layout>
    <x-slot name="title"></x-slot>
    <link rel="stylesheet" href="{{ asset('css/product.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    @livewireStyles

    {{$slot}}
    <script src="{{ asset('js/product.js') }}"></script>
    @livewireScripts
</x-ladmin-layout>
