<?php

namespace Modules\Product\Http\Livewire;

use App\Models\Product;
use Livewire\Component;

class ListProduct extends Component
{
    public $dataProduct;
    public $idProduct, $turnModal=false;
    public function turnModal($id){
        $this->idProduct = $id;
        $this->turnModal = true;
    }
    public function closeModal(){
        $this->turnModal = false;
    }
    public function acceptDelete(){
        if($this->idProduct){
            $data = Product::findOrFail($this->idProduct);
            $this->turnModal = false;
            if($data){
                $data->delete();
            }
        }
        return redirect()->back();
    }
    public function render()
    {
        $this->dataProduct = Product::get();
        return view('product::livewire.list-product')->layout('product::layouts.master');
    }
}
