<?php

namespace Modules\Product\Http\Livewire;

use App\Models\Category;
use App\Models\Product;
use Livewire\Component;

class CreateProduct extends Component
{
    public $name, $sale_price, $market_price, $summary, $category_id, $manufacturer, $description;
    public $dataCate;
    public $record_id = 0;
    protected $queryString = ['record_id'];
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function formSubmit(){
        if($this->record_id >0){
            $this->update();
        }else{
            $this->create();
        }
    }
    public function mount(){
        if($this->record_id >0 ){
            $dataProduct = Product::findOrFail($this->record_id);
            if($dataProduct){
                $this->name = $dataProduct->name;
                $this->sale_price = $dataProduct->sale_price;
                $this->market_price = $dataProduct->market_price;
                $this->summary = $dataProduct->summary;
                $this->description = $dataProduct->description;
                $this->manufacturer = $dataProduct->manufacturer;
                $this->category_id = $dataProduct->category_id;
            }
        }
    }
    public function update(){
        $this->rules();
        $dataProduct = Product::findOrFail($this->record_id);
        if($dataProduct){
            $dataProduct->fill([
                'name'=>$this->name,
                'sale_price'=>$this->sale_price,
                'market_price'=>$this->market_price,
                'summary'=>$this->summary,
                'description'=>$this->description,
                'manufacturer'=>$this->manufacturer,
                'category_id'=>$this->category_id
            ]);
        }
        if (!$dataProduct->clean) {
            $dataProduct->update();
            session()->flash('alert-success', 'Cập nhật thành công!');
            return redirect(route('product.list_product'));
        } else {
            return redirect()->back()->with('alert-error', "Cập nhật không thành công!");
        }
    }
    public function rules(){
        return [
            'name' => 'string|required|min:6',
            'description' => 'string|required|min:6',
            'manufacturer' => 'string|required|min:6',
            'sale_price' => 'required',
            'market_price' => 'required',
            'summary' => 'string|required|min:6',
            'category_id' => 'required',
        ];
    }
    public function create(){
        $this->validate();
        $dataProduct = Product::create([
            'name'=>$this->name,
            'sale_price'=>$this->sale_price,
            'market_price'=>$this->market_price,
            'summary'=>$this->summary,
            'description'=>$this->description,
            'manufacturer'=>$this->manufacturer,
            'category_id'=>$this->category_id
        ]);
        session()->flash('alert-success', 'Thêm mới thành công!');
        return redirect()->route('product.list_product');
    }
    public function render()
    {
        $this->dataCate = Category::get();
        return view('product::livewire.create-product')->layout('product::layouts.master');
    }
}
