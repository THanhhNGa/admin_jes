<?php

namespace Modules\Category\Http\Livewire;

use App\Models\Category;
use Livewire\Component;

class CreateCategory extends Component
{
    public $name, $description;
    public $record_id = 0;
    protected $queryString = ['record_id'];

    public function formSubmit(){
        if ($this->record_id > 0) {
            $this->update();
        } else {
            $this->create();
        }
    }
    public function rules()
    {
        return [
            'name' => 'string|required|min:6',
            'description' => 'string|required|min:6',

        ];
    }
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
    public function mount(){
        if($this->record_id > 0){
            $dataCate = Category::findOrFail($this->record_id);
            $this->name = $dataCate->name;
            $this->description = $dataCate->description;

        }
    }
    public function update(){
        $this->rules();
        $dataCate = Category::findOrFail($this->record_id);
        if($dataCate){
            $dataCate->fill([
                'name'=>$this->name,
                'description'=>$this->description
            ]);
        }
        if (!$dataCate->clean) {
            $dataCate->update();
            session()->flash('alert-success', 'Cập nhật thành công!');
            return redirect(route('category.list_category'));
        } else {
            return redirect()->back()->with('alert-error', "Cập nhật không thành công!");
        }
    }
    public function create(){
        $this->validate();
        $dataCate = Category::create([
           'name'=>$this->name,
            'description'=>$this->description
        ]);
        session()->flash('alert-success', 'Thêm mới thành công!');
        return redirect()->route('category.list_category');
    }
    public function render()
    {
        return view('category::livewire.create_category')->layout('category::layouts.master');
    }
}
