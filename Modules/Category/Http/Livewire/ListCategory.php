<?php

namespace Modules\Category\Http\Livewire;

use Livewire\Component;
use App\Models\Category;


class ListCategory extends Component
{
    public $listCategory;
    public $turnModal = false;
    public $idCate;
    public function turnModal($id){
        $this->turnModal = true;
        $this->idCate = $id;
    }
    public function closeModal(){
        $this->turnModal = false;
    }
    public function acceptDelete(){
    if($this->idCate){
        $data = Category::findOrFail($this->idCate);
        $this->turnModal = false;
        if($data){
            $data->delete();
        }
    }
    return redirect()->back();
}
    public function render()
    {
        $this->listCategory = Category::orderBy('id','desc')->get();
        return view('category::livewire.list_category',['dataCate'=>$this->listCategory])->layout('category::layouts.master');
    }


}
