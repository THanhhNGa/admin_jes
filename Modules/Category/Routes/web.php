<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('administrator/category')->name('category.')->group(function() {
    Route::get('/', [\Modules\Category\Http\Livewire\ListCategory::class,'__invoke'])->name('list_category');
    Route::get('/create', [\Modules\Category\Http\Livewire\CreateCategory::class, '__invoke'])->name('create_category');
});


