{{--@extends('category::layouts.master')--}}
{{--@section('style')--}}
{{--    <link rel="stylesheet" href="{{ asset('css/create-cate.css') }}">--}}
{{--@endsection--}}
{{--@section('content')--}}
<div>
        <x-slot name="title">Create Category</x-slot>
        <div id="create-category">
            <div class="wrap-category">
                <div class="option-category ">
                    <div class="option-1 ">
                    </div>
                </div>
            </div>
            <div class="main-category ">
                @if(session('alert-error'))
                    <div class="bg-red-300 border-t-4 border-red-500 rounded-b text-white px-4 py-3 shadow-md mb-2"
                         role="alert">
                        <div class="flex">
{{--                            <div class="icon-done">{!! showIcon('error') !!}</div>--}}
                            <div>
                                <p class="font-bold pl-1">{{session('alert-error')}}</p>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="main-content">
                    <div class="item-category">
                        <div class="row-attribute row-1">
                            <div class="title">Tên danh mục</div>
                            <div class="input-cate ">
                                <input wire:model="name" type="text" id="name" name="name">
                            </div>
                        </div>
                        <div class="row-attribute">
                            <div class="title">Mô tả</div>
                            <div class="input-cate">
                                <textarea wire:model="description" type="text" id="description" name="description"></textarea>
                            </div>
                        </div>
                        <div class="row-attribute">
                            <button type="submit"  wire:click.prevent="formSubmit" class="btn btn-info">Create</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

</div>
{{--@endsection--}}

