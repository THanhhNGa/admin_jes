{{--@extends('category::layouts.master')--}}
{{--@section('style')--}}
{{--    <link rel="stylesheet" href="{{ asset('css/category.css') }}">--}}
{{--@endsection--}}
{{--@section('content')--}}

<div>
    <x-slot name="buttons">List Category</x-slot>
    <div class="list-category">
        <div class="wrap-category">
            <div class="option-category ">
                <div class="title"><label>Danh sách danh mục</label></div>
                <div class="option-1 ">
                    <a href="{{route('category.create_category')}}" class="create btn btn-primary" title="create">Create</a>
                </div>
            </div>
        </div>
        @if(session('alert-success'))
            <div class="alert-success bg-blue-300 border-t-4 border-blue-500 rounded-b text-white px-4 py-3 shadow-md mb-2"
                 role="alert">
                <div class="flex">
                    <div>
                        <p class="font-bold pl-1">{{session('alert-success')}}</p>
                    </div>
                </div>
            </div>
        @endif
        <div class="main-category ">
            <div class="main-content">
                <div class="list-category">
                    <div class="item-category">
                        <div class="search">
                            <p>Show 10 entries</p>
                        </div>
                        <table class=" table-auto">
                            <thead>
                                <tr>
                                    <th class="">STT</th>
                                    <th class="col-name ">Tên danh mục</th>
                                    <th class="col-des w-1/6 ">Mô tả</th>
                                    <th class="col-option ">Tùy chọn</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($dataCate as $key=>$value)
                                <tr>
                                    <td>{{$loop->index +1}}</td>
                                    <td>{{$value->name}}</td>
                                    <td>{{$value->description}}</td>
                                    <td >
                                        <a href="{{route('category.create_category',['record_id'=>$value->id])}}" class="btn btn-info">Edit</a>
                                        <button type="button"   wire:click="turnModal({{$value->id}})" class="btn btn-primary"
                                                data-toggle="modal" data-target="#exampleModal" >Delete</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($turnModal)
        <x-category::category.From.modal wire:model="turnModal"/>
        @endif
    </div>
</div>
<script>
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })
</script>
{{--@endsection--}}

