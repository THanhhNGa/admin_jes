@props(['id'=>null, 'class'=>null])
<div class="modal fade " style="display: block; opacity: 1000; overflow: visible; top:100px" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bạn có chắc chắn muốn xóa không?</h5>
                <button type="button" wire:click="closeModal" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click="closeModal" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click="acceptDelete" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</div>
