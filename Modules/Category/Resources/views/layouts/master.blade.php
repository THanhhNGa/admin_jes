<x-ladmin-layout>
    <x-slot name="title"></x-slot>
    <link rel="stylesheet" href="{{ asset('css/create-cate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/category.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    @livewireStyles

    {{$slot}}
    <script src="{{ asset('js/category.js') }}"></script>
    @livewireScripts
</x-ladmin-layout>
