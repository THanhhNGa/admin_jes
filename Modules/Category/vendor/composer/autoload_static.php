<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit244630142df8b1970e57681fd9afa626
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Modules\\Category\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Modules\\Category\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit244630142df8b1970e57681fd9afa626::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit244630142df8b1970e57681fd9afa626::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit244630142df8b1970e57681fd9afa626::$classMap;

        }, null, ClassLoader::class);
    }
}
